
import java.util.List;
import java.util.Map;
import java.util.Set;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        
        Jogador player = new Jogador(1,"Fulano");
        time1.addJogador("Goleiro", player);
        
        player = new Jogador(1,"João");
        time2.addJogador("Goleiro", player);
        
        player = new Jogador(4,"Ciclano");
        time1.addJogador("Lateral", player);
        
        player = new Jogador(7,"José");
        time2.addJogador("Lateral", player); 
        
        player = new Jogador(10,"Beltrano");
        time1.addJogador("Atacante", player);
        
        player = new Jogador(15,"Mário");
        time2.addJogador("Atacante", player);
        
        Set<Map.Entry<String, Jogador>> entries1 = time1.getJogadores().entrySet();
        Set<Map.Entry<String, Jogador>> entries2 = time2.getJogadores().entrySet();
        
        for (Map.Entry<String, Jogador> entry1: entries1) {
           System.out.println(entry1.getKey() + ":\t" + time1.getJogadores().get(entry1.getKey()) + "\t" + time2.getJogadores().get(entry1.getKey()));
        }
        
        
        

        
    }
}
